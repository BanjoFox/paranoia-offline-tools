Skill List

-- Violence --
Athletics
Guns
Melee
Throw

-- Chutzpah --
Science
Psychology
Bureaucracy
Alpha Complex

-- Brains --
Bluff
Charm
Intimidate
Stealth

-- Mechanics --
Operate
Engineer
Program
Demolitions
