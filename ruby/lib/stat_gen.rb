#-
# Initialize all possible values on the sheet
#
VALUES = [-5, -4, -3, -2, -1, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5]

$shuffled = VALUES.shuffle;

#-
# This is useful for debugging
#
#puts "The shuffled values are #{$shuffled}"

sliced = $shuffled
  .each_slice(4)
  .map{ |s| s.count(&:positive?) }
  .shuffle;

#-
# This is useful for debugging
#
#puts "The sliced values are #{sliced}"

$violence = sliced[0]
$chutzpah = sliced[1]
$brains = sliced[2]
$mechanics = sliced[3]

