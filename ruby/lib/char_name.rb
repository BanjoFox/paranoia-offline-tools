##
# Some text
#
#
# -- BanjoFox
##

# Initialize some variables

CLEARANCE = [ *'R', 'O', 'Y', 'G', 'B','I', 'V']
SECTOR = [ *'A'..'Z' ]
CLONE = [ *'1'..'6' ]
em = "-"

#
# Name picker method
#
# Source URL: 
# http://stackoverflow.com/questions/11007111/ruby-whats-an-elegant-way-to-pick-a-random-line-from-a-text-file
#

def rand_line
  chosen_line = nil
  File.foreach("names.txt").each_with_index do |line, number|
    chosen_line = line if rand < 1.0/(number+1)
  end
  return chosen_line.strip
end

# Random Clearance picker (A-Z)
def clearance(len)
  rand_clearance = ''
    len.times { rand_clearance << CLEARANCE[rand(7)] }
  return rand_clearance
end

# Random Sector picker (A-Z)
def sector(len)
  rand_alpha = ''
    len.times { rand_alpha << SECTOR[rand(26)] }
  return rand_alpha
end

# Random clone picker (0-9)
def clone_num(len)
  rand_digit = ''
    len.times { rand_digit << CLONE[rand(6)] }
  return rand_digit
end

$character_name = rand_line + em + clearance(1) + em + sector(3) + em + clone_num(1)

#puts "The character name is: #{$character_name}"
