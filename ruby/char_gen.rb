##
#
# PARANOIA RPG (2016) CHARACTER CREATOR!!
#
#
#  Thanks to everyone on Stack Overflow for helping me out with this one!
#  http://stackoverflow.com/questions/39252750/using-ruby-how-can-i-get-a-count-for-all-positive-numbers-from-a-slice-of-an-ar/39253567#39253567
#
# -- BanjoFox
##

require_relative 'lib/char_name.rb'
require_relative 'lib/stat_gen.rb'

#- 
# Output a reasonable facsimilie of a character sheet!
# :D
#

puts("
-------------------
Character Name:
-------------------
Name: #{$character_name}

-------------------
Stats:
-------------------
Violence: #{$violence}, Chutzpah: #{$chutzpah}, Brains: #{$brains}, Mechanics: #{$mechanics}

-------------------
Skills:
-------------------
Athletics: #{$shuffled[0]}	Science: #{$shuffled[4]}	   Bluff: #{$shuffled[8]}  	  Operate: #{$shuffled[12]}
Guns: #{$shuffled[1]} 	Psychology: #{$shuffled[5]} 	   Charm: #{$shuffled[9]}  	  Engineer: #{$shuffled[13]}
Melee: #{$shuffled[2]}	Bureaucracy: #{$shuffled[6]}	   Intimidate: #{$shuffled[10]}  Program: #{$shuffled[14]}
Throw: #{$shuffled[3]}	Alpha\ Complex: #{$shuffled[7]}   Stealth: #{$shuffled[11]}    Demolitions: #{$shuffled[15]}

")
