import random

VALUES = ['-5', '-4', '-3', '-2', '-1', '0', '0', '0', '0', '0', '0', '1', '2', '3', '4', '5']
random.shuffle(VALUES)

print "The shuffled values are ", VALUES


# Yield successive n-sized chunks from l. 
def slicer(l, n): 
      
    # looping till length l 
    for i in range(0, len(l), n):  
        yield l[i:i + n] 
  
# How many elements each 
# list should have 
n = 4
  
STATS = list(slicer(VALUES, n)) 
print "The STAT groupings are ", STATS 


#sliced = shuffled
#  .each_slice(4)
#  .map{ |s| s.count(&:positive?) }
#  .shuffle;

#puts "The sliced values are ", sliced
#
