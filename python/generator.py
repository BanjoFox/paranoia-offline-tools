#!/usr/bin/python
#
# This is a VERY simple Python script to generate character names for Paranoia.
# All names follow the convention of Firstname-SingleLetter-AlphaNumeric(times 3)
# 	Example: ALAN-R-7R2
#
# The list of names was taken from a US Census list of Male and Female first names
#
# 	Source URL: http://deron.meranda.us/data/census-derived-all-first.txt
#
# There are probably ways to simplify the code, but for now it works
#
# Don't forget... Fun is Mandatory!
# 	- BanjoFox!
#

import string
import random

# Open the name file as read-only
fopen = open("names.txt", "r")
get_name = file.readlines(fopen) 

# Randomize and pick some letters to create a Sector name within Alpha Complex
def clearance_suffix(size=0, clearance=['R', 'O', 'Y', 'G', 'B', 'I', 'V']):
	sector = ''.join(random.choice(clearance) for _ in range(1))
	return sector

# Randomize and pick some letters to create a Sector name within Alpha Complex
def sector_suffix(size=0, sector=string.ascii_uppercase):
	sector = ''.join(random.choice(sector) for _ in range(3))
	return sector

# Finally, pick a clone number
def clone_num(size=1, clones=['1', '2', '3', '4', '5', '6']):
	return ''.join(random.choice(clones) for _ in range(1))


# Prompt for user input
usr_in = input("How many names would you like? ")

# Create a number of names until the value equals the user selected value
count = 0 
while count < usr_in:

	# Combine and print the name
	print random.choice(get_name).strip() + "-" + clearance_suffix() + "-" + sector_suffix() + "-" + clone_num()
	count +=1

# Cleanup 
fopen.close()
