#- 
# Import random, der
#
from random import randint

#-
# Initialize the dice_result list
#
#dice_result = []

#-
# Initialize counters for dice values ( >5< )
#
#num_not_success = 0
#num_success = 0

#-
# Get the players NODE
#
def get_node():
	print( "What is your NODE Citizen?" )
	NODE = int( input() )

	if NODE > 0:
		print( "Excellent work Citizen, your NODE means that you cannot reasonably fail.", NODE )
	elif NODE == 0:
		print( "Would you like some help Citizen?", NODE )
	else:
		print( "Citizen, Please report to R&D for a MANDATORY UP-GRADE to your Cerebral CoreTech.", NODE )
	return NODE

#-
# Get absolute value of NODE, and roll dice :)
#
def roll_NODE(NODE):
	NODE_count = abs(NODE)
	dice_result = []
	while NODE_count > 0 :
		dice_result.append( int( randint(1, 6) ))
		NODE_count -= 1
	return dice_result

#-
# Lets get Friend Computer involved! :D
#
def roll_Computer():
	computer_dice = int( randint(1, 6))
	if computer_dice > 5:
		success = True
	else:
		success = False
	return success, computer_dice

#-
# Count number of successes, and non-successes
# That way we can do math later, if NODE is negative
#
def count_success():
	NODE = get_node()
	dice_result = roll_NODE(NODE)
	num_not_success = sum(map(lambda b: 1 if b < 5 else 0, dice_result))
	num_success = sum(map(lambda b: 1 if b >= 5 else 0, dice_result))
	croll, cdice = roll_Computer()
	if croll: 
		num_success += 1
	else:
		num_not_success += 1

	print( "Number of Not Successes: ", num_not_success )
	print( "Number of Successes: ", num_success )

	if NODE < 0:
		outcome = num_success - num_not_success
	else:
		outcome = num_success

	print( "You rolled:", dice_result )
	print( "The outcome is:", outcome )

	if cdice < 6:
		print( "Computer Dice is: ", cdice)
	else:
		print( "Computer Dice is: COMPUTER")
	#return outcome

if __name__ == "__main__": # we're running it from the command line
	count_success()
