# Paranoia Name Generator
Paranoia is a tabletop RPG currently owned by Mongoose Publishing in the UK, and has a special place in my heart because of it's humorous play-style, and concept.  
The new version (2016-2017) includes a unique character creation scheme where -all- the players are involved in the process.  Solo creation is possible but not 
as much fun.  The current CLI tool just generates names based on the Alpha Complex Syntax [name]-[clearance]-[sector]-[clone_#]

Currently there are two command line versions.  One is written in Ruby, the other is written in Python.

## Ruby
- Generates a random name
- Populates all character stats using random values

**Usage**
```
$ cd ruby
$ ruby char_gen.rb
```

## Python
- Generates a user-selectable number of names
- No stat generation yet.

**Usage**
```
cd ruby
$ python generator.py
```

